##					TAREA 1: Predictores de saltos condicionales**

En esta tarea se implementó la simulación del esparcimiento de un virus, traducido del lenguage NetLogo.
  
--------------------------------------------
--------------------------------------------

**Dependencias**

*  En el directorio `/B54557_T6/include` encontrará los headers del código fuente.
*  En el directorio `/B54557_T6/scr` encontrará los archivos del código fuente.
*  Para compilar el archivo necesitará gcc.
*  Para generar los gráficos necesitará de un complilador de R, se recomienda usar el IDE RStudio.


--------------------------------------------
--------------------------------------------

**Compilación** 

En la terminal de comandos en el directorio `/B54557_T6`:

1. Ejecutar la instrucción `make`
2. Encontrará el ejecutable creado en el directorio `/B54557_T6/simulation` con el nombre `P1`. 

--------------------------------------------
--------------------------------------------

**Ejecución**

En la terminal de comandos en el directorio `/B54557_T6/simulation` escribir:

`./P1 -lsp <#> -cr <#> -cc <#> -imd <#> -inf <#> -dt <#> -rec <#> -np <#> -tck <#>`

Para generar los gráficos diríjase al directorio `/B54557_T6/output`, luego abra y ejecute el archivo `Graphs.Rmd`.


## Flags
---

* -lsp	:	lifespan, the lifespan of a turtle.
* -cr	:	chance_reproduce, the probability of a turtle generating an offspring each tick.
* -cc	:	carrying_capacity, the number of turtles that can be in the world at one time.
* -imd	:	immunity_duration, how many weeks immunity lasts.
* -inf	:	infectiousness, the probability of a turtle to get sick.
* -dt	:	duration, how many weeks of sick before recover or die.
* -rec	:	chance_recover, chance to recover after sick.
* -np	:	number_people, initial number of people.
* -tck	:	ticks, number of weeks for simulation.



## Outs
---
* En el directorio `/B54557_T6/simulation/output` encontrará los archivos de salida.



## Inputs
---
* No se requieren inputs además de los flas, estos pueden no ser definidos, en ese caso los valores por defecto seran usados.


**Referencia**

1. Wilensky, U. (1998). NetLogo Virus model. http://ccl.northwestern.edu/netlogo/models/Virus. Center for Connected Learning and Computer Based Modeling, Northwestern University, Evanston, IL. 



