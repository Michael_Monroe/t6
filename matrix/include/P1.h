#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>


typedef struct turtles{

    bool sick;
    unsigned int remaining_immunity;
    unsigned int sick_time;
    unsigned int age;

} turtles;

typedef struct cell{

    turtles* T0;
    bool valid;

} cell;


typedef struct globals{

    float infected;                 //what % of the population is infectious
    float immune;                   //what % of the population is immune
    unsigned int lifespan;          //the lifespan of a turtle
    unsigned int chance_reproduce;  //the probability of a turtle generating an offspring each tick
    unsigned int carrying_capacity; //the number of turtles that can be in the world at one time
    unsigned int immunity_duration; //how many weeks immunity lasts
    unsigned int infectiousness;    //the probability of a turtle to get sick
    unsigned int duration;          //how many weeks of sick before recover or die
    unsigned int chance_recover;    //chance to recover after sick
    unsigned int number_people;     //Initial number of people
    unsigned int ticks;             //number of weeks for simulation
    } globals;


globals getGlobals(int argc, char const* argv[]);

void get_sick(turtles* T1);

void get_healthy(turtles* T1);

void become_immune(turtles* T1, globals* G0);

int get_older(turtles* T1, globals* G0);

int get_move();

int infect(turtles* T1, globals* G0);

int recover_or_die(turtles* T1, globals* G0);

turtles* reproduce(int count_turtles, globals* G0);

void kill_turtle(turtles* C1);

turtles* born_turtle();

turtles* create_turtle(globals* G0);




